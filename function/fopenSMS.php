<?php
function chechSender($userAccount,$passAccount,$senderId)
{
	global $hostName;
	global $path;
	$contextPostValues = http_build_query(array('mobile'=>$userAccount, 'password'=>$passAccount, 'senderId'=>$senderId));
	$contextOptions['http'] = array('method' => 'POST', 'header'=>'Content-type: application/x-www-form-urlencoded', 'content'=> $contextPostValues, 'max_redirects'=>0, 'protocol_version'=> 1.0, 'timeout'=>10, 'ignore_errors'=>TRUE);
	$contextResouce  = stream_context_create($contextOptions);
	$url = "http://".$hostName.$path."/checkSender.php";
	$handle = fopen($url, 'r', false, $contextResouce);
	$result = stream_get_contents($handle);
	return $result;
}
function activeSender($userAccount, $passAccount, $senderId, $activeKey)
{
	global $hostName;
	global $path;
	if(strstr($senderId,'#'))
	{
		$str = explode("#",$senderId);
		$senderId = $str[1];
	}
	$contextPostValues = http_build_query(array('mobile'=>$userAccount, 'password'=>$passAccount, 'senderId'=>$senderId, 'activeKey'=>$activeKey));
	$contextOptions['http'] = array('method' => 'POST', 'header'=>'Content-type: application/x-www-form-urlencoded', 'content'=> $contextPostValues, 'max_redirects'=>0, 'protocol_version'=> 1.0, 'timeout'=>10, 'ignore_errors'=>TRUE);
	$contextResouce  = stream_context_create($contextOptions);
	$url = "http://".$hostName.$path."/activeSender.php";
	$handle = fopen($url, 'r', false, $contextResouce);
	$result = stream_get_contents($handle);
	return $result;
}
function addSender($userAccount, $passAccount, $sender, $siteSender)
{
	global $hostName;
	global $path;
	$contextPostValues = http_build_query(array('mobile'=>$userAccount, 'password'=>$passAccount, 'sender'=>$sender, 'siteSender'=>$siteSender));
	$contextOptions['http'] = array('method' => 'POST', 'header'=>'Content-type: application/x-www-form-urlencoded', 'content'=> $contextPostValues, 'max_redirects'=>0, 'protocol_version'=> 1.0, 'timeout'=>10, 'ignore_errors'=>TRUE);
	$contextResouce  = stream_context_create($contextOptions);
	$url = "http://".$hostName.$path."/addSender.php";
	$handle = fopen($url, 'r', false, $contextResouce);
	$result = stream_get_contents($handle);
	return $result;
}
function sendSMS($userAccount, $passAccount, $numbers, $sender, $msg, $MsgID, $timeSend=0, $dateSend=0)
{
	global $hostName;
	global $path;
	global $applicationType;  
	$msg = convertToUnicode($msg);
	$sender = urlencode($sender);
	$domainName = $_SERVER['SERVER_NAME'];
	$contextPostValues = http_build_query(array('mobile'=>$userAccount, 'password'=>$passAccount, 'numbers'=>$numbers, 'sender'=>$sender, 'msg'=>$msg, 'timeSend'=>$timeSend, 'dateSend'=>$dateSend, 'applicationType'=>$applicationType, 'domainName'=>$domainName, 'msgId'=>$MsgID));
	$contextOptions['http'] = array('method' => 'POST', 'header'=>'Content-type: application/x-www-form-urlencoded', 'content'=> $contextPostValues, 'max_redirects'=>0, 'protocol_version'=> 1.0, 'timeout'=>10, 'ignore_errors'=>TRUE);
	$contextResouce  = stream_context_create($contextOptions);
	$url = "http://".$hostName.$path."/msgSend.php";
	$handle = fopen($url, 'r', false, $contextResouce);
	$result = stream_get_contents($handle);
	return $result;
}
function sendSMSWK($userAccount, $passAccount, $numbers, $sender, $msg, $msgKey, $timeSend=0, $dateSend=0, $deleteKey=0)
{
	global $hostName;
	global $path;
	global $applicationType;  
	$msg = convertToUnicode($msg);
	$sender = urlencode($sender);
	$domainName = $_SERVER['SERVER_NAME'];
	$contextPostValues = http_build_query(array('mobile'=>$userAccount, 'password'=>$passAccount, 'numbers'=>$numbers, 'sender'=>$sender, 'msg'=>$msg, 'msgKey'=>$msgKey, 'timeSend'=>$timeSend, 'dateSend'=>$dateSend, 'applicationType'=>$applicationType, 'domainName'=>$domainName, 'deleteKey'=>$deleteKey));
	$contextOptions['http'] = array('method' => 'POST', 'header'=>'Content-type: application/x-www-form-urlencoded', 'content'=> $contextPostValues, 'max_redirects'=>0, 'protocol_version'=> 1.0, 'timeout'=>10, 'ignore_errors'=>TRUE);
	$contextResouce  = stream_context_create($contextOptions);
	$url = "http://".$hostName.$path."/msgSend.php";
	$handle = fopen($url, 'r', false, $contextResouce);
	$result = stream_get_contents($handle);
	return $result;
}
function balanceSMS($userAccount, $passAccount)
{
	global $hostName;
	global $path;
	$contextPostValues = http_build_query(array('mobile'=>$userAccount, 'password'=>$passAccount));
	$contextOptions['http'] = array('method' => 'POST', 'header'=>'Content-type: application/x-www-form-urlencoded', 'content'=> $contextPostValues, 'max_redirects'=>0, 'protocol_version'=> 1.0, 'timeout'=>10, 'ignore_errors'=>TRUE);
	$contextResouce  = stream_context_create($contextOptions);
	$url = "http://".$hostName.$path."/balance.php";
	$handle = fopen($url, 'r', false, $contextResouce);
	$result = stream_get_contents($handle);
	list($accountOriginal, $account) = explode("/", $result);
	return "your account $account from $accountOriginal point";
}
?>