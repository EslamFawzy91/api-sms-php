<?php
function chechSender($userAccount,$passAccount,$senderId)
{
	global $hostName;
	global $path;
	$stringToPost = "mobile=".$userAccount."&password=".$passAccount."&senderId=".$senderId;
	$stringToPostLength = strlen($stringToPost);
	$fsockParameter = "POST $path/checkSender.php HTTP/1.0 \r\n";
	$fsockParameter.= "Host: $hostName \r\n";
	$fsockParameter.= "Content-type: application/x-www-form-urlencoded \r\n";
	$fsockParameter.= "Content-length: $stringToPostLength \r\n";
	$fsockParameter.= "$stringToPost \r\n";
	$fsockConn = fsockopen($hostName, 80, &$errno, &$errstr, 30);
	fputs($fsockConn,$fsockParameter);
	$result = ""; 
	$clearResult = false; 
	while(!feof($fsockConn))
	{
		$line = fgets($fsockConn, 10240);
		if($line == "\r\n" && !$clearResult)
			$clearResult = true;
		if($clearResult)
			$result .= trim($line); 
	}
	return $result;
}
function activeSender($userAccount, $passAccount, $senderId, $activeKey)
{
	global $hostName;
	global $path;
	if(strstr($senderId,'#'))
	{
		$str = explode("#",$senderId);
		$senderId = $str[1];
	}
	$stringToPost = "mobile=".$userAccount."&password=".$passAccount."&senderId=".$senderId."&activeKey=".$activeKey;
	$stringToPostLength = strlen($stringToPost);
	$fsockParameter = "POST $path/activeSender.php HTTP/1.0 \r\n";
	$fsockParameter.= "Host: $hostName \r\n";
	$fsockParameter.= "Content-type: application/x-www-form-urlencoded \r\n";
	$fsockParameter.= "Content-length: $stringToPostLength \r\n";
	$fsockParameter.= "$stringToPost \r\n";
	$fsockConn = fsockopen($hostName, 80, &$errno, &$errstr, 30);
	fputs($fsockConn,$fsockParameter);
	$result = ""; 
	$clearResult = false; 
	while(!feof($fsockConn))
	{
		$line = fgets($fsockConn, 10240);
		if($line == "\r\n" && !$clearResult)
			$clearResult = true;
		if($clearResult)
			$result .= trim($line); 
	}
	return $result;
}
function addSender($userAccount, $passAccount, $sender, $siteSender)
{
	global $hostName;
	global $path;
	$stringToPost = "mobile=".$userAccount."&password=".$passAccount."&sender=".$sender."&siteSender=".$siteSender;
	$stringToPostLength = strlen($stringToPost);
	$fsockParameter = "POST $path/addSender.php HTTP/1.0 \r\n";
	$fsockParameter.= "Host: $hostName \r\n";
	$fsockParameter.= "Content-type: application/x-www-form-urlencoded \r\n";
	$fsockParameter.= "Content-length: $stringToPostLength \r\n\r\n";
	$fsockParameter.= "$stringToPost"; 
	$fsockConn = fsockopen($hostName, 80, &$errno, &$errstr, 30);
	fputs($fsockConn,$fsockParameter);
	$result = ""; 
	$clearResult = false; 
	while(!feof($fsockConn))
	{
		$line = fgets($fsockConn, 10240);
		if($line == "\r\n" && !$clearResult)
			$clearResult = true;
		if($clearResult)
			$result .= trim($line); 
	}
	return $result;
}
function sendSMS($userAccount, $passAccount, $numbers, $sender, $msg, $MsgID, $timeSend=0, $dateSend=0)
{
	global $hostName;
	global $path;
	global $applicationType;
	$msg = convertToUnicode($msg);
	$sender = urlencode($sender);
	$domainName = $_SERVER['SERVER_NAME'];
	$stringToPost = "mobile=".$userAccount."&password=".$passAccount."&numbers=".$numbers."&sender=".$sender."&msg=".$msg."&timeSend=".$timeSend."&dateSend=".$dateSend."&applicationType=".$applicationType."&domainName=".$domainName."&msgId=".$MsgID;
	$stringToPostLength = strlen($stringToPost);
	$fsockParameter = "POST $path/msgSend.php HTTP/1.0 \r\n";
	$fsockParameter.= "Host: $hostName \r\n";
	$fsockParameter.= "Content-type: application/x-www-form-urlencoded \r\n";
	$fsockParameter.= "Content-length: $stringToPostLength \r\n\r\n";
	$fsockParameter.= "$stringToPost";
	$fsockConn = fsockopen($hostName, 80, &$errno, &$errstr, 30);
	fputs($fsockConn, $fsockParameter);
	$result = ""; 
	$clearResult = false; 
	while(!feof($fsockConn))
	{
		$line = fgets($fsockConn, 10240);
		if($line == "\r\n" && !$clearResult)
			$clearResult = true;
		if($clearResult)
			$result .= trim($line); 
	}
	return $result;
}
function sendSMSWK($userAccount, $passAccount, $numbers, $sender, $msg, $msgKey, $timeSend=0, $dateSend=0, $deleteKey=0)
{
	global $hostName;
	global $path;
	global $applicationType;
	$msg = convertToUnicode($msg);
	$sender = urlencode($sender);
	$domainName = $_SERVER['SERVER_NAME'];
	$stringToPost = "mobile=".$userAccount."&password=".$passAccount."&numbers=".$numbers."&sender=".$sender."&msg=".$msg."&msgKey=".$msgKey."&timeSend=".$timeSend."&dateSend=".$dateSend."&applicationType=".$applicationType."&domainName=".$domainName."&deleteKey=".$deleteKey;
	$stringToPostLength = strlen($stringToPost);
	$fsockParameter = "POST $path/msgSend.php HTTP/1.0 \r\n";
	$fsockParameter.= "Host: $hostName \r\n";
	$fsockParameter.= "Content-type: application/x-www-form-urlencoded \r\n";
	$fsockParameter.= "Content-length: $stringToPostLength \r\n\r\n";
	$fsockParameter.= "$stringToPost";
	$fsockConn = fsockopen($hostName, 80, &$errno, &$errstr, 30);
	fputs($fsockConn, $fsockParameter);
	$result = ""; 
	$clearResult = false; 
	while(!feof($fsockConn))
	{
		$line = fgets($fsockConn, 10240);
		if($line == "\r\n" && !$clearResult)
			$clearResult = true;
		if($clearResult)
			$result .= trim($line); 
	}
	return $result;
}
function balanceSMS($userAccount, $passAccount)
{
	global $hostName;
	global $path;
	$stringToPost = "mobile=".$userAccount."&password=".$passAccount;
	$stringToPostLength = strlen($stringToPost);
	$fsockParameter = "POST $path/balance.php HTTP/1.0 \r\n";
	$fsockParameter.= "Host: $hostName \r\n";
	$fsockParameter.= "Content-type: application/x-www-form-urlencoded \r\n";
	$fsockParameter.= "Content-length: $stringToPostLength \r\n\r\n";
	$fsockParameter.= "$stringToPost";
	$fsockConn = fsockopen($hostName, 80, &$errno, &$errstr, 5);
	fputs($fsockConn,$fsockParameter);
	$result = ""; 
	$clearResult = false; 
	while(!feof($fsockConn))
	{
		$line = fgets($fsockConn, 10240);
		if($line == "\r\n" && !$clearResult)
			$clearResult = true;
		if($clearResult)
			$result .= trim($line); 
	}
	list($accountOriginal, $account) = explode("/", $result);
	return "your account $account from $accountOriginal point";
}
?>